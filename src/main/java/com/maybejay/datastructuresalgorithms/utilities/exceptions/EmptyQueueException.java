package com.maybejay.datastructuresalgorithms.utilities.exceptions;

public class EmptyQueueException extends Exception {
	public EmptyQueueException(String message) {
		super(message);
	}
}
