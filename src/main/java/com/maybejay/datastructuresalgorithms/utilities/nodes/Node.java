package com.maybejay.datastructuresalgorithms.utilities.nodes;

public abstract class Node<E> {
	protected E data;

	public Node(E data) {
		this.data = data;
	}
}
