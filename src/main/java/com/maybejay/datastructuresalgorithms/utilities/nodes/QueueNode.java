package com.maybejay.datastructuresalgorithms.utilities.nodes;

public class QueueNode<E> extends Node<E>{
	private QueueNode<E> next;
	private QueueNode<E> prev;

	public QueueNode(E data) {
		super(data);
	}

	public QueueNode(E data, QueueNode<E> next) {
		super(data);
		this.next = next;
	}

	public void setNext(QueueNode<E> next) {
		this.next = next;
	}

	public void setPrev(QueueNode<E> prev) {
		this.prev = prev;
	}

	public E getData() {
		return this.data;
	}

	public QueueNode<E> getNext() {
		return this.next;
	}

	public QueueNode<E> getPrev() {
		return this.prev;
	}

	public boolean incremenetHead() {
		if (this.next != null) {
			this.next = this.next.next;
			return true;
		}
		return false;
	}
}
